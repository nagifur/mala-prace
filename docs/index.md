# Ukázková Dokumentace

Pomocí mkdocs [mkdocs.org](https://www.mkdocs.org).

## Projekt

* `mkdocs==1.3.0` - MKdocs verze
* `Python 3.10` - Verze Pythonu
* ...
* `requirements.txt` - Všechny využité moduly

## Nainstalovaný motiv

* `mkdocs-material==8.2.15`
* `mkdocs-material-extensions==1.0.3`

