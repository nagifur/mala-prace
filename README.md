# mala-prace

Část závěrečné práce ze SSP

## Použití

Ve složce docs je uložen markdown ze které je následně pomocí pipeliny vygenerován statický web, stačí jej tedy
upravit a poté dojde k automatické kontrole zdrojáků a deployi.

## Prerekvizity

Doporučeno spustit ve virtuálnim prostředí (venv). Doinstalovat potřebné balíky uvedené v requirements.txt

*`pip install requirements.txt`

## Závislosti

Python 3.10

Uvedeny v requirements.txt
